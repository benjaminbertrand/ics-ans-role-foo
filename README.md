# ics-ans-role-foo

Ansible role to install foo.


## Requirements

- ansible >= 2.4
- molecule >= 2.6

## Role Variables

```yaml
...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-foo
```

## License

BSD 2-clause
